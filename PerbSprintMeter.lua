-----------------------------------------------------------------------------------------------
-- Client Lua Script for PerbSprintMeter
-- Copyright (c) NCsoft. All rights reserved
-----------------------------------------------------------------------------------------------
require "Window"
require "GameLib"
require "Apollo"

local PerbSprintMeter = {}

function PerbSprintMeter:new(o)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function PerbSprintMeter:Init()
	Apollo.RegisterAddon(self)
end

function PerbSprintMeter:OnLoad()
	self.xmlDoc = XmlDoc.CreateFromFile("PerbSprintMeter.xml")
	self.xmlDoc:RegisterCallback("OnDocumentReady", self) 
end

function PerbSprintMeter:OnDocumentReady()
	if  self.xmlDoc == nil then	return end
	
	Apollo.LoadSprites("BorderSprite.xml", "Perb")
	Apollo.RegisterEventHandler("VarChange_FrameCount", "OnFrame", self)

	self.wndMain = Apollo.LoadForm(self.xmlDoc, "Background", "InWorldHudStratum", self)
	self.wndMain:Show(false, true)
end

function PerbSprintMeter:OnFrame()
	local unitPlayer = GameLib.GetPlayerUnit()
	if not unitPlayer then return end

	local nRunCurr = unitPlayer:GetResource(0)
	local nRunMax = unitPlayer:GetMaxResource(0)
	local bAtMax = nRunCurr == nRunMax or unitPlayer:IsDead()

	self.wndMain:FindChild("SprintMeter"):SetMax(nRunMax)
	self.wndMain:FindChild("SprintMeter"):SetProgress(nRunCurr)

	self.wndMain:Show(not bAtMax, true)
end

local PerbSprintMeterInst = PerbSprintMeter:new()
PerbSprintMeterInst:Init()
